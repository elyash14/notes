# git-code
My Git command

* Get Origin of repo
	1. All information : git remote show origin
	2. Just URL: git config --get remote.origin.url

* Change origin of repo
	1. git remote rm origin
	2. git remote add origin git@github.com:test/proj1.git
	3. git config master.remote origin
	4. git config master.merge refs/heads/master

* Get current branch
	1. git branch
	2. git pull [origin name] [branch name]