Having a well tested backend is an important step for having a successful product. Most of the business logic is there, and you’ll want to make sure that neither a “future you” or someone else will break the beautiful code you’ve written today.

Therefore, it is never too late to stop and revise some of the best practices when it comes to software testing:

a) Be familiar with the testing pyramid
We usually divide automated tests into three categories: unit, integration and end-to-end tests. Unit tests are smaller and simpler: they are meant to test one internal method at a time, without contacting the external world. Integration tests encompass bigger methods and calls to external services (which, however, should be mocked). Finally, end-to-end tests serve as an extensive check into your routes, to see if everything’s ok in real conditions.

The testing pyramid has been described with some different shapes, but the main idea remains universal: we should write more of the simpler tests and less of the more complicated ones, without leaving any of them behind.

b) KISS
The KISS (Keep It Simple, Stupid) principle applies to tests as well. Testing can be very time-consuming, so we should avoid at all costs over-engineering and losing time, right?

Not all logic needs to be tested! Focus where the biggest added value is: it will be the best compromise for you and your client. As always, quality matters more than quantity.

c) Write code that it’s easy to maintain
The best practice is to test the result of your method, not the implementation details. When finishing writing a test, ask yourself: if I ever refactor the tested method (without changing the result), will I have to modify this test? If the answer is yes, you can simplify your tests.

d) Give a chance to Test Driven Development (TDD)
TDD is a development technique that lots of people have heard of (and even tried once), but don’t use that often. When you start each chunk of logic of your code with a test that fails and then you write the minimal amount of code that makes it pass, you’re ensuring that you’re not over-engineering your app and that your tests are useful and reliable.


Jest cheatSheet : https://devhints.io/jest