#Linux User Management Commands

who:
    who is a command to show all user that logged in currently

last:
    show last users that logged in before
    example: last | awk '{print $1}' | sort | uniq

w: 
    is a commend like 'who' but with more information

id:
    get user info by this command
